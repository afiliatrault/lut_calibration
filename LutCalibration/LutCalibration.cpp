// LutCalibration.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


//#include <opencv\cv.h>   // This is the original code, but I couldn't get VideoCapture work correctly.
#include <opencv2/opencv.hpp>
#include <opencv\highgui.h>

#include <opencv2/video.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <fstream>

using namespace cv;
using namespace std;

int KEY_ESCAPE = 27;
int KEY_ENTER = 13;
int KEY_SPACE = 32;



bool WriteImageData(const Mat& _image, const string& filename)
{
    Mat image1Channel;

    if(3 == _image.channels())
    {
        cvtColor(_image, image1Channel, CV_BGR2GRAY);
    }
    else if(4 == _image.channels())
    {
        cvtColor(_image, image1Channel, CV_BGRA2GRAY);
    }
    else if(1 == _image.channels())
    {
        image1Channel = _image;
    }
    else
    {
        cerr << "Bad input image!";
        return false;
    }

    char *data = reinterpret_cast<char*>(image1Channel.data);
    int nbPixels = _image.rows * _image.cols;

    ofstream myFile(filename, ios::out | ios::binary);
    myFile.write(data, nbPixels);
    myFile.close();

    return true;
}

Mat ReadImageData(string& _filename, int rows, int cols)
{
    ifstream inStream(_filename, ios::in | ios::binary);
    Mat imageReRead = cv::Mat(rows, cols, CV_8UC1);

    char* reRead2 = reinterpret_cast<char*>(imageReRead.data);
    inStream.read(reRead2, rows*cols);
    inStream.close();

    return imageReRead;
}

int main()
{
    const int KEY_ESCAPE = 27;
    const int KEY_ENTER = 13;
    const int KEY_SPACE = 32;

    const int PATTERN_HEIGHT = 24;
    const int PATTERN_WIDTH = 32;

    Mat board_image = imread("fc2_save_2016-12-08-141022-0000_26pouces3_4.tif");
    Mat corrected_board_image = board_image.clone();

    cv::Mat channel[3];

    split(board_image, channel);


    int channels = board_image.channels();

    Size patternsize(PATTERN_HEIGHT, PATTERN_WIDTH); //interior number of corners

    vector<Point2f> corners; //this will be filled by the detected corners
                             //CALIB_CB_FAST_CHECK saves a lot of time on images
                             //that do not contain any chessboard corners
    bool patternfound = findChessboardCorners(board_image, patternsize, corners,
        CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
        + CALIB_CB_FAST_CHECK);

//     if (patternfound)
//         cornerSubPix(channel[0], corners, patternsize, Size(-1, -1),
//             TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 300, 0.01));

    cv::Mat showable_image = board_image.clone();
    drawChessboardCorners(showable_image, patternsize, Mat(corners), patternfound);
    imshow("Found!", showable_image);
    cv::waitKey(30);
    imwrite("chessboard.png", showable_image);

    cout << "Hello Chessboard!!" << endl << endl;
    //cout << corners << endl;

    Mat cornerMatX(PATTERN_HEIGHT, PATTERN_WIDTH, CV_32FC1);
    Mat cornerMatY(PATTERN_HEIGHT, PATTERN_WIDTH, CV_32FC1);

    Mat idxImageX(PATTERN_HEIGHT, PATTERN_WIDTH, CV_32FC1);
    Mat idxImageY(PATTERN_HEIGHT, PATTERN_WIDTH, CV_32FC1);
    Mat outputImageX = board_image.clone();
    Mat outputImageY = board_image.clone();

    int cornerIterator = 0;
    for (auto iCol = 0; iCol < PATTERN_WIDTH; ++iCol)
    {
        for (auto iRow = PATTERN_HEIGHT - 1; iRow >= 0; --iRow)
        {
            cornerMatX.at<float>(iRow, iCol) = corners.at(cornerIterator).x;
            cornerMatY.at<float>(iRow, iCol) = corners.at(cornerIterator++).y;
            idxImageX.at<float>(iRow, iCol) = 50*iCol;
            idxImageY.at<float>(iRow, iCol) = 50*iRow;
        }
    }

    Mat enlargedIdxX = board_image.clone();
    Mat enlargedIdxY = board_image.clone();
    Mat enlargedcornerMatX = board_image.clone();
    Mat enlargedcornerMatY = board_image.clone();

    resize(idxImageX, enlargedIdxX, board_image.size());
    resize(idxImageY, enlargedIdxY, board_image.size());
    resize(cornerMatX, enlargedcornerMatX, board_image.size());
    resize(cornerMatY, enlargedcornerMatY, board_image.size());

    cv::remap(enlargedIdxX, outputImageX, enlargedcornerMatX, enlargedcornerMatY, INTER_LINEAR);
    cv::remap(enlargedIdxY, outputImageY, enlargedcornerMatX, enlargedcornerMatY, INTER_LINEAR);
    cv::remap(board_image, corrected_board_image, outputImageX, outputImageY, INTER_LINEAR);

    cout << "cornerMatY: " << cornerMatY << endl;

    imshow("Corrected!", corrected_board_image);
    cv::waitKey(30);


    while (1)
    {
        auto key = cv::waitKey(30);

        if (KEY_ENTER == key) // ENTER
        {
            imwrite("Corrected.png", corrected_board_image);
            cout << "Corrected.png written! "<< endl;
            break;
        }
    }

    Mat testImage = imread("E:\\tmp\\TestImage.png");
    cv::remap(testImage, corrected_board_image, outputImageX, outputImageY, INTER_LINEAR);

    imshow("Corrected test!", corrected_board_image);
    cv::waitKey(30);

    while (1)
    {
        auto key = cv::waitKey(30);

        if (KEY_ENTER == key) // ENTER
        {
            imwrite("CorrectedTest.png", corrected_board_image);
            cout << "CorrectedTest.png written! " << endl;
            break;
        }
    }














//     while (1) {
//         cap >> image;          //copy webcam stream to image
// 
//         imshow("Current color image feed", image);          //print image to screen
//         auto key = waitKey(33);          //delay 33ms
// 
//         if (KEY_ENTER == key) // ESCAPE
//         {
//             WriteImageData(image, imgName);
//             image1Channel = ReadImageData(imgName, image.rows, image.cols);
//             imshow("Saved BW Image", image1Channel);          //print image to screen
// 
//             cout << "Image Saved to " + imgName << endl;
// 
//         }
// 
//         if (KEY_ESCAPE == key) // ESCAPE
//         {
//             cap.release();
//             break;
//         }
// 
//     }

    return 0;
}
